import 'tailwindcss/tailwind.css'
import { AppProps } from 'next/app'
import '../styles/globals.css'
import { SWRConfig } from 'swr'
import pRetry from 'p-retry'

const fetchUrl = (url) => fetch(url).then((r) => r.json())

const fetcher = (url) => pRetry(() => fetchUrl(url), { retries: 5 })

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <SWRConfig
      value={{
        fetcher,
        revalidateOnFocus: false,
      }}
    >
      <Component {...pageProps} />
    </SWRConfig>
  )
}

export default MyApp
