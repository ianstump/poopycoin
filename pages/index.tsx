import Dashboard from '../containers/Dashboard'
import { getShitcoins } from './api/getShitcoins'
import pRetry from 'p-retry'

export const Home = (): JSX.Element => {
  return (
    <div>
      <header>
        <div className="relative bg-white">
          <div className=" mx-auto px-4 sm:px-6">
            <div className="flex justify-between items-center border-b-2 border-gray-100 py-6 md:justify-start md:space-x-10">
              <h1>Welcome!</h1>
            </div>
          </div>
        </div>
      </header>

      <Dashboard />
    </div>
  )
}

export default Home

export async function getStaticProps() {
  const topShittyCoins = await pRetry(getShitcoins, { retries: 5 })

  if (!topShittyCoins) {
    return {
      notFound: true,
    }
  }

  return {
    props: {
      fallback: {
        '/api/getShitcoins': topShittyCoins,
      },
    }, // will be passed to the page component as props
  }
}
