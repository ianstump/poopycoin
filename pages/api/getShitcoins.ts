// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import { NextApiRequest, NextApiResponse } from 'next'
import pRetry from 'p-retry'
import { Coin, PoopyCoin } from '../../lib/types'

export const TOKEN = 'e13bec80-04b8-46a2-9115-b9aa031203ba'

export const getShitcoins = async (): Promise<PoopyCoin[]> => {
  const response = await fetch('https://api.coincap.io/v2/assets', {
    headers: { Authorization: `Bearer ${TOKEN}` },
  })
  const { data } = await response.json()

  const reversedAarray: Coin[] = data.reverse() || []

  const topShittyCoins = reversedAarray
    .sort((a, b) => Number(b.rank) - Number(a.rank))
    .filter((coin, index) => index < 30)
    .map(({ id, symbol, name }) => ({ id, symbol, name }))

  return topShittyCoins
}

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const topShitcoins = await pRetry(getShitcoins, { retries: 5 })

  res.status(200).json(topShitcoins)
}

export default handler
