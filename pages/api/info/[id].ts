// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import { NextApiRequest, NextApiResponse } from 'next'
import pRetry from 'p-retry'

export const TOKEN = 'e13bec80-04b8-46a2-9115-b9aa031203ba'

const getBitcoin = async (id) => {
  const response = await fetch(`https://api.coincap.io/v2/rates/${id}`, {
    headers: { Authorization: `Bearer ${TOKEN}` },
  })
  const { data } = await response.json()

  return data
}

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const { id } = req.query

  const bitcoinData = await pRetry(() => getBitcoin(id), { retries: 10 })

  res.status(200).json(bitcoinData)
}

export default handler
