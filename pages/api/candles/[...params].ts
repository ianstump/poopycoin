import { TOKEN } from '../getShitcoins'

const URL = 'https://api.coincap.io/v2/candles'

// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import { NextApiRequest, NextApiResponse } from 'next'
import pRetry from 'p-retry'

export const getCandles = async (id, period) => {
  const params = `?exchange=poloniex&interval=${period}&baseId=${id}&quoteId=bitcoin`
  const response = await fetch(URL + params, {
    headers: { Authorization: `Bearer ${TOKEN}` },
  })
  const data = await response.json()

  return data
}

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const { params } = req.query

  const [id, period] = params as string[]

  const data = await pRetry(() => getCandles(id, period), {
    retries: 10,
  })

  res.status(200).json(data)
}

export default handler
