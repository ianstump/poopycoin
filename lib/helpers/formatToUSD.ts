const formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
})

export default function formatToUSD(priceInDollars: number) {
  return formatter.format(priceInDollars)
}
