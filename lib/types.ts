export type Coin = {
  id: string
  rank: string
  symbol: string
  name: string
  supply: string
  maxSupply: string
  marketCapUsd: string
  volumeUsd24Hr: string
  priceUsd: string
  changePercent24Hr: string
  vwap24Hr: string
}

export type PoopyCoin = Pick<Coin, 'id' | 'symbol' | 'name'>

export type RealTimePrice = {
  [coinId: string]: string
}

export type Candle = {
  open: string
  high: string
  low: string
  close: string
  volume: string
  period: string
}

export type History = {
  priceUsd: string
  time: number
}
