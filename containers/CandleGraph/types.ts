export enum PeriodTypes {
  m1 = '1 minute',
  m5 = '5 minutes',
  m15 = '15 minutes',
  m30 = '30 minutes',
  h1 = '1 hour',
  h2 = '2 hours',
  h4 = '4 hours',
  h8 = '8 hours',
  h12 = '12 hours',
  d1 = '1 day',
  w1 = '1 week',
}
