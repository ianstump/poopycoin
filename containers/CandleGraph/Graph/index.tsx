import {
  Bar,
  BarChart,
  CartesianGrid,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts'

import dayjs from 'dayjs'
import { PeriodTypes } from '../types'

const CustomTooltip = ({ label }: { label: string }) => {
  return <div>date: {dayjs(label).format('llll')}</div>
}

function formatXAxis(tickItem) {
  return dayjs(tickItem).format('D.MMM.YY')
}

const selectOptions = Object.entries(PeriodTypes).map(([key, label]) => ({
  key,
  label,
}))

export default function Graph({ setPeriod, graphData }) {
  const onChange = ({ target }) => {
    return setPeriod(target.value)
  }

  if (graphData.length === 0) {
    return (
      <div className="mb-10">
        Looks like the server is not returning any graph data :(
      </div>
    )
  }

  return (
    <div style={{ height: 200, width: 500, marginBottom: 50 }}>
      <label className="flex justify-between">
        Select Time Period for the graph
        <select name="select period" onChange={onChange}>
          {selectOptions.map(({ key, label }) => (
            <option key={key} value={key}>
              {label}
            </option>
          ))}
        </select>
      </label>

      <ResponsiveContainer>
        <BarChart data={graphData}>
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis
            tickCount={100}
            scale="time"
            interval="preserveStart"
            dataKey="name"
            tickFormatter={formatXAxis}
            angle={-25}
          />
          <YAxis domain={['dataMin', 'dataMax']} />
          <Tooltip content={CustomTooltip} />
          <Bar dataKey="value" fill="#8884d8" />
        </BarChart>
      </ResponsiveContainer>
    </div>
  )
}
