import useSWR from 'swr'

import dayjs from 'dayjs'
import { useEffect, useMemo, useState } from 'react'
import localizedFormat from 'dayjs/plugin/localizedFormat'
import Graph from './Graph'
import { PeriodTypes } from './types'
import Stats from './Stats'
import { Candle, PoopyCoin } from '../../lib/types'

dayjs.extend(localizedFormat)

const constructData = (data: Candle[] = []) => {
  return data.map(({ period, high, low }) => {
    return {
      name: period,
      value: [low, high],
    }
  })
}

type CandleResponse = {
  data: Candle[]
}

export default function CandleGraph({ id }: { id: PoopyCoin['id'] }) {
  const [idToFetch, setIdToFetch] = useState(id)
  const [period, setPeriod] = useState<PeriodTypes>(
    Object.keys(PeriodTypes)[0] as PeriodTypes
  )
  const { data: response, error } = useSWR<CandleResponse>(
    `api/candles/${idToFetch}/${period}`
  )

  const graphData = useMemo(
    () => (Array.isArray(response?.data) ? constructData(response?.data) : []),
    [response?.data, constructData]
  )

  const hasNoData = response && response?.data?.length === 0 && id === idToFetch
  const isLoading = !response && !error

  useEffect(() => {
    if (hasNoData) setIdToFetch('bitcoin')
  }, [hasNoData, setIdToFetch])

  if (id !== idToFetch)
    return (
      <div>
        <p className="text-2xl">
          this 💩coin is so poopy I can&apos;t find any data
        </p>
        <p className="text-gray-600 mb-5">Here is the Bitcoin info for now:</p>
        <Stats id={idToFetch} />

        <Graph graphData={graphData} setPeriod={setPeriod} />
      </div>
    )

  if (isLoading) {
    return <div className="pb-10">Loading profile...</div>
  }

  return (
    <div>
      <Stats id={idToFetch} />

      <Graph graphData={graphData} setPeriod={setPeriod} />
    </div>
  )
}
