import useSWR from 'swr'
import { PoopyCoin, RealTimePrice } from '../../../lib/types'
import Cards from './Cards'

export default function Stats({ id }: { id: PoopyCoin['id'] }): JSX.Element {
  const { data, error } = useSWR<RealTimePrice>(`api/info/${id}`)
  const loading = !data && !error

  if (loading) return <div>...loading</div>

  const { currencySymbol, rateUsd, symbol } = data || {}

  return (
    <div>
      <h5 className="w-full text-2xl font-semibold text-center">{symbol}</h5>

      <Cards id={id} currencySymbol={currencySymbol} rateUsd={rateUsd} />
    </div>
  )
}
