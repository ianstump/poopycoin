import Card from '../../../../components/Card'
import { memo } from 'react'
import LivePrice from '../../../ShitCoinRow/Price'

type Props = {
  currencySymbol: string
  rateUsd: string
  id: string
}

export default memo(({ currencySymbol, rateUsd, id }: Props) => {
  return (
    <div className="flex justify-between pt-5">
      <Card title="Symbol:" subtitle={currencySymbol || 'No symbol'} />
      <Card
        title="Current Price:"
        subtitle={<LivePrice id={id} priceUsd={rateUsd} />}
      />
    </div>
  )
})
