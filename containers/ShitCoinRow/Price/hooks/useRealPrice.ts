import { useCallback, useEffect, useState } from 'react'
import formatToUSD from '../../../../lib/helpers/formatToUSD'

type Props = { priceUsd: string; id: string; shouldCancelWS?: boolean }

export function useRealPrice({ priceUsd, id }: Props) {
  const [realTimePrice, setRealTimePrice] = useState(priceUsd)
  const [changeType, setChangeType] = useState(0)

  const socket = new WebSocket(`wss://ws.coincap.io/prices?assets=${id}`)

  const onOpen = () => socket.send('Knock knock')

  const onMessage = useCallback(
    (event) => {
      const newPrice = JSON.parse(event.data)[id]
      setChangeType(Math.sign(Number(realTimePrice) - Number(newPrice)))
      setRealTimePrice(newPrice)
    },
    [setChangeType, realTimePrice, setRealTimePrice]
  )

  useEffect(() => {
    socket.addEventListener('open', onOpen)
    socket.addEventListener('message', onMessage)

    return () => {
      socket.removeEventListener('open', onOpen)
      socket.removeEventListener('message', onMessage)
      socket.close()
    }
  }, [socket, onOpen, onMessage])

  return {
    changeType,
    realPrice: formatToUSD(Number(realTimePrice)),
  }
}
