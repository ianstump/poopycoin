import { memo } from 'react'
import styled from 'styled-components'
import { useRealPrice } from './hooks/useRealPrice'
import { Coin } from '../../../lib/types'

function PriceComponent({
  priceUsd,
  id,
}: Pick<Coin, 'priceUsd' | 'id'> & { shouldCancelWS?: boolean }) {
  const { realPrice, changeType } = useRealPrice({
    priceUsd,
    id,
  })

  return <Price changeType={changeType}>{realPrice}</Price>
}

const LivePrice = memo(PriceComponent)

export default LivePrice

export const Price = styled.div`
  box-shadow: inherit;
  transition: box-shadow 1000ms ease-in;
  color: ${({ changeType }) => {
    if (changeType === 0) return 'blue'
    if (changeType === 1) return 'green'
    if (changeType === -1) return 'red'
  }};
`
