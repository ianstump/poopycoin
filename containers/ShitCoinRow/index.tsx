import useSWRImmutable from 'swr/immutable'
import LastMonthGraph from './LastMonthGraph'
import Price from './Price'
import formatToUSD from '../../lib/helpers/formatToUSD'
import { Coin } from '../../lib/types'
import { get } from 'lodash'

type Props = {
  name: string
  symbol: string
  id: string
  openCoinProfile: (id: string) => void
}

export const Skeleton = () => {
  return (
    <div className="flex animate-pulse flex-row items-center h-full justify-center space-x-5">
      <div className="w-24 bg-gray-300 h-6 rounded-md" />
    </div>
  )
}

export default function ShitCoinRow({
  name,
  symbol,
  id,
  openCoinProfile,
}: Props) {
  const { data: response, error } = useSWRImmutable<{ data: Coin }>(
    'https://api.coincap.io/v2/assets/' + id
  )
  const loading = !response && !error

  const coin = get(response, 'data', {})

  const { changePercent24Hr, marketCapUsd, priceUsd, volumeUsd24Hr } = coin

  if (!response) return null

  return (
    <tr className="odd:bg-red">
      <td className="px-6 py-4 whitespace-nowrap">
        <div className="text-gray-900">
          <a
            onClick={() => openCoinProfile(id)}
            className="flex items-center underline cursor-pointer text-purple-600"
          >
            {name}
          </a>
        </div>
      </td>
      <td className="px-6 py-4 whitespace-nowrap">
        <div className="text-gray-500">{symbol}</div>
      </td>

      <td className="px-6 py-4 whitespace-nowrap">
        {loading ? <Skeleton /> : <Price priceUsd={priceUsd} id={id} />}
      </td>

      <td className="px-6 py-4 whitespace-nowrap">
        <div className="text-gray-500">{formatToUSD(marketCapUsd)}</div>
      </td>

      <td className="px-6 py-4 whitespace-nowrap">
        <div className="text-gray-500">{formatToUSD(volumeUsd24Hr)}</div>
      </td>

      <td className="px-6 py-4 whitespace-nowrap">
        {loading ? null : (
          <LastMonthGraph id={id} changePercent24Hr={changePercent24Hr} />
        )}
      </td>
    </tr>
  )
}
