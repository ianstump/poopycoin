import { Line, ResponsiveContainer, XAxis } from 'recharts'
import { LineChart } from 'recharts'
import useSWRImmutable from 'swr/immutable'
import { Skeleton } from '..'
import { History } from '../../../lib/types'

export default function LastMonthGraph({
  id,
  changePercent24Hr,
}: {
  id: string
  changePercent24Hr: string
}) {
  const { data, error } = useSWRImmutable<{ data: History[] }>(
    'https://api.coincap.io/v2/assets/' + id + '/history?interval=d1'
  )

  if (!data && !error) return <Skeleton />

  const isNegative = Math.sign(Number(changePercent24Hr)) === -1

  const percentageChange = Number(changePercent24Hr).toFixed(4)

  const strokeColor = isNegative ? 'red' : 'black'

  return (
    <div
      className="flex items-center"
      style={{ width: '200px', height: '100px' }}
    >
      <ResponsiveContainer width="100%" height="100%">
        <LineChart margin={{ top: 50 }} data={data?.data}>
          <XAxis tick={false} axisLine={false} dataKey="time" />
          <Line
            isAnimationActive={false}
            dot={false}
            type="monotone"
            dataKey="priceUsd"
            stroke={strokeColor}
          />
        </LineChart>
      </ResponsiveContainer>

      <span className={`text-${strokeColor}-500 pl-10`}>
        {percentageChange} %
      </span>
    </div>
  )
}
