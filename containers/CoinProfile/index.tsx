import CandleGraph from '../CandleGraph'
import { PoopyCoin } from '../../lib/types'

export default function Profile({
  closePortal,
  id,
}: {
  closePortal: () => void
  id: PoopyCoin['id']
}) {
  return (
    <div className="fixed z-10 inset-0 overflow-y-auto">
      <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div
          className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity -my-5"
          aria-hidden="true"
        />
        <div className="p-20 my-20 inline-block bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all">
          <CandleGraph id={id} />

          <button
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
            onClick={closePortal}
          >
            Close me!
          </button>
        </div>
      </div>
    </div>
  )
}
