import useSWR from 'swr'
import ShitCoinRow from '../ShitCoinRow'
import usePortal from 'react-useportal'
import Profile from '../CoinProfile'
import { useCallback, useState } from 'react'
import { PoopyCoin } from '../../lib/types'

export default function Dashboard() {
  const [openedCoin, setOpenedCoin] = useState<PoopyCoin['id']>(null)
  const { data: options = [] } = useSWR<PoopyCoin[]>('/api/getShitcoins')
  const { openPortal, closePortal, isOpen, Portal, ref } = usePortal()

  const openCoinProfile = useCallback(
    (id: PoopyCoin['id']) => {
      setOpenedCoin(id)
      openPortal()
    },
    [setOpenedCoin, openPortal]
  )

  return (
    <>
      {isOpen && (
        <Portal>
          <Profile closePortal={closePortal} id={openedCoin} />
        </Portal>
      )}

      <div
        ref={ref}
        className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg"
      >
        <table className=" min-w-full table-auto divide-y divide-gray-200 border-separate">
          <thead className="bg-gray-50">
            <th
              scope="col"
              className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
            >
              Name
            </th>
            <th
              scope="col"
              className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
            >
              Ticker
            </th>
            <th
              scope="col"
              className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
            >
              Price USD
            </th>
            <th
              scope="col"
              className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
            >
              Market cap
            </th>
            <th
              scope="col"
              className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
            >
              Volume
            </th>
            <th
              scope="col"
              className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
            >
              Last Month Graph
            </th>
          </thead>
          <tbody className="divide-y divide-gray-100">
            {options.map(({ symbol, id, name }) => (
              <ShitCoinRow
                openCoinProfile={openCoinProfile}
                key={id}
                id={id}
                name={name}
                symbol={symbol}
              />
            ))}
          </tbody>
        </table>
      </div>
    </>
  )
}
