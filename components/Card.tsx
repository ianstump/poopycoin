import { ReactNode } from 'react'

export default function Card({
  title,
  subtitle,
}: {
  title: ReactNode
  subtitle: ReactNode
}): JSX.Element {
  return (
    <div>
      <div className="w-full bg-white h-auto tracking-wide mb-14 border border-black-800 mx-1 rounded-lg relative">
        <div className="small-banner w-1 h-full bg-blue-600 absolute rounded-tl-md" />
        <div className="flex items-center justify-between p-5">
          <h5 className="font-semibold">{title}</h5>
          <p className="font-regular text-gray-500">{subtitle}</p>
        </div>
      </div>
    </div>
  )
}
